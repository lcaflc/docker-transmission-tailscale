FROM lscr.io/linuxserver/transmission:latest as build

RUN apk --no-cache add tailscale py3-netifaces rpcbind iftop tcpdump nload htop 

#RUN wget -O /usr/bin/getnetwork https://raw.githubusercontent.com/BeardedTek-com/tailscale/main/rootfs/usr/bin/getnetwork && \
#    sed -i 's/venv/usr/' /usr/bin/getnetwork && \
#    chmod +x /usr/bin/getnetwork

COPY root /
